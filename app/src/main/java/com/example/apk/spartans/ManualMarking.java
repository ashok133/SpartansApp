package com.example.apk.spartans;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by APK on 03/01/18.
 */

public class ManualMarking extends Activity {

    public EditText rollNumberEntry;
    public Button add, delete, submit;
    public TextView rollNumbersList;

    public ArrayList<Integer> rollNumbers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manual_marking);

        rollNumberEntry = findViewById(R.id.rollNumberEntry);
        add = findViewById(R.id.addButton);
        delete = findViewById(R.id.deleteButton);
        submit = findViewById(R.id.submitButton);
        rollNumbersList = findViewById(R.id.rollNumbersList);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String roll = rollNumberEntry.getText().toString();
                // TODO: fix condition checks
                if (roll!=null && !rollNumbers.contains(Integer.parseInt(roll))){
                    //int rollNumber = Integer.parseInt(roll);
                    StringBuilder sb = new StringBuilder();
                        //rollNumbers.set(rollNumbers.size(), Integer.parseInt(roll));
                    rollNumbers.add(Integer.parseInt(roll));
                    for (Integer number : rollNumbers) {
                            sb.append (number + "\n");
                    }
                    rollNumbersList.setText(sb.toString());
                    rollNumberEntry.setText("");
                }
                else {
                    toaster("Please enter a valid & new roll number.");
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String roll = rollNumberEntry.getText().toString();
                if (roll!=null && rollNumbers.contains(Integer.parseInt(roll))){
                    //int rollNumber = Integer.parseInt(roll);
                    StringBuilder sb = new StringBuilder();
                    //rollNumbers.set(rollNumbers.size(), Integer.parseInt(roll));
                    int elementIndex = rollNumbers.indexOf(Integer.parseInt(roll));
                    rollNumbers.remove(elementIndex);
                    for (Integer number : rollNumbers) {
                        sb.append (number + "\n");
                    }
                    rollNumbersList.setText(sb.toString());
                    rollNumberEntry.setText("");
                }
                else {
                    toaster("Roll Number not in list! Add it first.");
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toaster("Attendance for today will be marked!");
                finish();
            }
        });
    }
    public void toaster (final String message) {
        ManualMarking.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ManualMarking.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
