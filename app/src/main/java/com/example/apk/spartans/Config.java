package com.example.apk.spartans;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by APK on 12/12/17.
 */

public class Config extends Activity{

    // to be replaced with
//    public static String FILE_UPLOAD_URL = "http://192.168.43.41/AndroidFileUpload/fileUpload.php";
    public static String MARK_ATTENDANCE_URL = "http://tech-challenge-spartans.faenpnk2np.us-east-1.elasticbeanstalk.com/api/v1/markAttendance";
    public static String INDEX_FACE_URL = "http://tech-challenge-spartans.faenpnk2np.us-east-1.elasticbeanstalk.com/api/v1/indexFace";
    public static String SEARCH_FACE_URL = "http://tech-challenge-spartans.faenpnk2np.us-east-1.elasticbeanstalk.com/api/v1/searchFace";

    // Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "Android File Upload";

    public Button setSF, cancelSF, setMA, cancelMA;
    public EditText SFEditor, MAEditor;

    public String fileUploadUrl;

    public TextView currSFURL, currMAURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ip_changer);

        setSF = findViewById(R.id.setButtonSF);
        cancelSF = findViewById(R.id.cancelButtonSF);
        setMA = findViewById(R.id.setButtonMA);
        cancelMA = findViewById(R.id.cancelButtonMA);

        SFEditor = findViewById(R.id.newSearchFaceURL);
        MAEditor = findViewById(R.id.newMarkAttendanceURL);

        currSFURL = findViewById(R.id.currURLSearchFace);
        currMAURL = findViewById(R.id.currURLMarkAttendance);

        currSFURL.setText(SEARCH_FACE_URL);
        currMAURL.setText(Config.MARK_ATTENDANCE_URL);

        setSF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SFEditor.getText().toString()!=null) {
                    fileUploadUrl = SFEditor.getText().toString();
                    Config.SEARCH_FACE_URL = fileUploadUrl;
                    currSFURL.setText(fileUploadUrl);
                    toaster("Server URL Updated!");
                }
                else {
                    // Toast.makeText(MainActivity.this, "Invalid or Null IP", Toast.LENGTH_SHORT);
                    toaster("Invalid or Null IP");
                }
            }
        });

        cancelSF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        setMA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MAEditor.getText().toString()!=null) {
                    fileUploadUrl = MAEditor.getText().toString();
                    Config.MARK_ATTENDANCE_URL = fileUploadUrl;
                    currMAURL.setText(fileUploadUrl);
                    toaster("Server URL Updated!");
                }
                else {
                    // Toast.makeText(MainActivity.this, "Invalid or Null IP", Toast.LENGTH_SHORT);
                    toaster("Invalid or Null IP");
                }
            }
        });

        cancelMA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void toaster (final String message) {
        Config.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(Config.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }


}
