package com.example.apk.spartans;

/**
 * Created by APK on 12/12/17.
 */


import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

import com.example.apk.spartans.*;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

public class UploadActivity extends Activity {

    // LogCat tag
    private static final String TAG = MainActivity.class.getSimpleName();

    public ProgressDialog pd, pd2;

    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;


    private String filePath = null;
    private ImageView imgPreview;
    private ImageView decodedImgPreview;
    private VideoView vidPreview;
    private Button btnUpload, btnRetake;
    private TextView timestamp;
    private TextView brightnessLevel;
    private TextView faceCount;
    private Spinner subjectSelector;
    private Spinner semesterSelector;
    private Spinner divSelector;

    // data to be uploaded: base64 string, semester, div, sub, timestamp
    public String encodedImageString;
    public String semester, division, subject ;

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    OkHttpClient client = new OkHttpClient();

    public String markAttendanceUrl = Config.MARK_ATTENDANCE_URL;
    public String searchFaceUrl = Config.SEARCH_FACE_URL;
    public String indexFaceUrl = Config.INDEX_FACE_URL;

    public FileInputStream fin = null;

    long totalSize = 0;

    UploadActivity upload ;

    String[] semesters = {"Select semester","I","II","III","IV"};

    String[] divs = {"Select division","COMP (A)","MECH (B)", "IT (C)"};

    String[] sem1_subjects = {"Select subject","AMI","API","ACI","BEE"};
    String[] sem2_subjects = {"Select subject","AMII","APII","ACII","EM"};
    String[] sem3_subjects = {"Select subject","OOPM","DS","DSGT","COA"};
    String[] sem4_subjects = {"Select subject","AMIV","RDBMS","MP","AOA"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        //txtPercentage = findViewById(R.id.txtPercentage);

        btnUpload = findViewById(R.id.btnUpload);
        btnUpload.setEnabled(false);

        btnRetake = findViewById(R.id.btnRetake);

        //progressBar = findViewById(R.id.progressBar);
        imgPreview          = findViewById(R.id.imgPreview);
        vidPreview          = findViewById(R.id.videoPreview);
        subjectSelector     = findViewById(R.id.subjectSpinner);
        semesterSelector    = findViewById(R.id.semesterSpinner);
        divSelector         = findViewById(R.id.divisionSpinner);
        timestamp           = findViewById(R.id.timestamp);
        brightnessLevel     = findViewById(R.id.brightnessText);
        faceCount           = findViewById(R.id.faceCount);

        upload = UploadActivity.this;

        ArrayAdapter semAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,semesters){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be used for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        semAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        semesterSelector.setAdapter(semAdapter);
        semesterSelector.setSelection(0);
        semesterSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                semester = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if(position > 0){

                    // -----------------------------DIVISION SPINNER-----------------------------------------------------------------
                    ArrayAdapter divAdapter = new ArrayAdapter(UploadActivity.this,android.R.layout.simple_spinner_item,divs){
                        @Override
                        public boolean isEnabled(int position){
                            if(position == 0)
                            {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        @Override
                        public View getDropDownView(int position, View convertView,
                                                    ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if(position == 0){
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            }
                            else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;
                        }
                    };
                    divAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    divSelector.setAdapter(divAdapter);
                    divSelector.setSelection(0);
                    divSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            division = parent.getItemAtPosition(position).toString();
                            // If user change the default selection
                            // First item is disabled and it is used for hint
                            if(position > 0){
                                // do nothing
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    // -----------------------------DIVISION SPINNER ENDS------------------------------------------------------------

                    // Dynamically changing the subjects based on semester
                    // -----------------------------SUBJECT SPINNERS-----------------------------------------------------------------
                    switch (semester)
                    {
                        case "I": {
                            ArrayAdapter aa = new ArrayAdapter(UploadActivity.this,android.R.layout.simple_spinner_item,sem1_subjects){
                                @Override
                                public boolean isEnabled(int position){
                                    if(position == 0)
                                    {
                                        // Disable the first item from Spinner
                                        // First item will be use for hint
                                        return false;
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                }
                                @Override
                                public View getDropDownView(int position, View convertView,
                                                            ViewGroup parent) {
                                    View view = super.getDropDownView(position, convertView, parent);
                                    TextView tv = (TextView) view;
                                    if(position == 0){
                                        // Set the hint text color gray
                                        tv.setTextColor(Color.GRAY);
                                    }
                                    else {
                                        tv.setTextColor(Color.BLACK);
                                    }
                                    return view;
                                }
                            };
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            subjectSelector.setAdapter(aa);
                            subjectSelector.setSelection(0);
                            subjectSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    subject = parent.getItemAtPosition(position).toString();
                                    // If user change the default selection
                                    // First item is disable and it is used for hint
                                    if(position > 0){
                                        // Notify the selected item text
                                        // Toast.makeText(getApplicationContext(), "Selected Subject: " + subject, Toast.LENGTH_SHORT).show();
                                        btnUpload.setEnabled(checkParams());
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            break;
                        }

                        case "II": {
                            ArrayAdapter aa = new ArrayAdapter(UploadActivity.this,android.R.layout.simple_spinner_item,sem2_subjects){
                                @Override
                                public boolean isEnabled(int position){
                                    if(position == 0)
                                    {
                                        // Disable the first item from Spinner
                                        // First item will be use for hint
                                        return false;
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                }
                                @Override
                                public View getDropDownView(int position, View convertView,
                                                            ViewGroup parent) {
                                    View view = super.getDropDownView(position, convertView, parent);
                                    TextView tv = (TextView) view;
                                    if(position == 0){
                                        // Set the hint text color gray
                                        tv.setTextColor(Color.GRAY);
                                    }
                                    else {
                                        tv.setTextColor(Color.BLACK);
                                    }
                                    return view;
                                }
                            };
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            subjectSelector.setAdapter(aa);
                            subjectSelector.setSelection(0);
                            subjectSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    subject = (String) parent.getItemAtPosition(position).toString();
                                    // If user change the default selection
                                    // First item is disable and it is used for hint
                                    if(position > 0){
                                        // Notify the selected item text
                                        // Toast.makeText(getApplicationContext(), "Selected Subject: " + subject, Toast.LENGTH_SHORT).show();
                                        btnUpload.setEnabled(checkParams());
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            break;
                        }

                        case "III": {
                            ArrayAdapter aa = new ArrayAdapter(UploadActivity.this,android.R.layout.simple_spinner_item,sem3_subjects){
                                @Override
                                public boolean isEnabled(int position){
                                    if(position == 0)
                                    {
                                        // Disable the first item from Spinner
                                        // First item will be use for hint
                                        return false;
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                }
                                @Override
                                public View getDropDownView(int position, View convertView,
                                                            ViewGroup parent) {
                                    View view = super.getDropDownView(position, convertView, parent);
                                    TextView tv = (TextView) view;
                                    if(position == 0){
                                        // Set the hint text color gray
                                        tv.setTextColor(Color.GRAY);
                                    }
                                    else {
                                        tv.setTextColor(Color.BLACK);
                                    }
                                    return view;
                                }
                            };
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            subjectSelector.setAdapter(aa);
                            subjectSelector.setSelection(0);
                            subjectSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    subject = (String) parent.getItemAtPosition(position).toString();
                                    // If user change the default selection
                                    // First item is disable and it is used for hint
                                    if(position > 0){
                                        // Notify the selected item text
                                        // Toast.makeText(getApplicationContext(), "Selected Subject: " + subject, Toast.LENGTH_SHORT).show();
                                        btnUpload.setEnabled(checkParams());
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            break;
                        }

                        case "IV": {
                            ArrayAdapter aa = new ArrayAdapter(UploadActivity.this,android.R.layout.simple_spinner_item,sem4_subjects){
                                @Override
                                public boolean isEnabled(int position){
                                    if(position == 0)
                                    {
                                        // Disable the first item from Spinner
                                        // First item will be use for hint
                                        return false;
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                }
                                @Override
                                public View getDropDownView(int position, View convertView,
                                                            ViewGroup parent) {
                                    View view = super.getDropDownView(position, convertView, parent);
                                    TextView tv = (TextView) view;
                                    if(position == 0){
                                        // Set the hint text color gray
                                        tv.setTextColor(Color.GRAY);
                                    }
                                    else {
                                        tv.setTextColor(Color.BLACK);
                                    }
                                    return view;
                                }
                            };
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            subjectSelector.setAdapter(aa);
                            subjectSelector.setSelection(0);
                            subjectSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    subject = (String) parent.getItemAtPosition(position).toString();
                                    // If user change the default selection
                                    // First item is disable and it is used for hint
                                    if(position > 0){
                                        // Notify the selected item text
                                        // Toast.makeText(getApplicationContext(), "Selected Subject: " + subject, Toast.LENGTH_SHORT).show();
                                        btnUpload.setEnabled(checkParams());
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            break;
                        }
                        default: Log.e("spinner default","something's wrong");
                    }
                    // -----------------------------SUBJECT SPINNERS END-----------------------------------------------------------------
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // pass
            }
        });

        Calendar calendar = Calendar.getInstance();
        timestamp.setText(String.format("%1$tI:%1$tM %1$Tp, %1$tA %1$tb %1$td, %1$tY ", calendar));

        // Receiving the data from previous activity
        Intent i = getIntent();

        // image or video path that is captured in previous activity
        filePath = i.getStringExtra("filePath");
        Log.e("Upload filepath",filePath);

        // boolean flag to identify the media type, image or video
        boolean isImage = i.getBooleanExtra("isImage", true);

        if (filePath != null) {
            // Displaying the image or video on the screen
            previewMedia(isImage);
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
        }

        btnUpload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // uploading the file to server
                new SearchFaces().execute();
            }
        });

        btnRetake.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
                File file = new File(filePath);
                file.delete();
            }
        });

    }

    //Displaying captured image/video on the screen
    @SuppressWarnings("deprecation")
    private void previewMedia(boolean isImage) {
        // Checking whether captured media is image or video
        if (isImage) {
            imgPreview.setVisibility(View.VISIBLE);
            vidPreview.setVisibility(View.GONE);

            // bitmap factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // to ensure our bitmap canvas is drawable for rectangles
            options.inMutable = true;

            // down sizing image as it throws OutOfMemory Exception for larger images
            options.inSampleSize = 8;

            Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

            // check for brightness level and give approporiate suggestion
            brightnesschecker(bitmap);

            Bitmap bitmapForRes = BitmapFactory.decodeFile(filePath);
            int widthN = bitmapForRes.getWidth();
            Log.e("Width",Float.toString(widthN));

            if (widthN > 1440) {
                // rescale image and then obtain base64 string for image data
                encodedImageString = reScaleAndEncode(bitmap, 1440);
            }
            else {
                // obtain base64 string for image data directly
                encodedImageString = base64Converter();
            }


            Bitmap tempBitmap = drawRects(filePath, options);

            imgPreview.setImageDrawable(new BitmapDrawable(getResources(),tempBitmap));
        }

        else {
            imgPreview.setVisibility(View.GONE);
            vidPreview.setVisibility(View.VISIBLE);
            vidPreview.setVideoPath(filePath);
            vidPreview.start();
        }
    }


    private class SearchFaces extends AsyncTask<Void, Integer, String> {

        @SuppressWarnings("deprecation")
        @Override
        protected void onPreExecute() {

            pd = new ProgressDialog(UploadActivity.this);
            pd.setTitle("Uploading captured image");
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.setIndeterminate(true);
            pd.show();

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {
            if (isInternetAvailable())
                return uploadImageData();
            else {
                toaster("Internet connection not available, your data has been saved for uploading later.");
                return "Couldn't establish a reliable server connection. Will try again later!";
            }
        }

        @SuppressWarnings("deprecation")
        private String uploadImageData() {

            // TODO:
            // configure column names, Configure other APIs, format json string properly

            JSONObject searchFaceJSON = new JSONObject();
            try {
                searchFaceJSON = new JSONObject();
                searchFaceJSON.put("collection_id","test");
                searchFaceJSON.put("image_data",encodedImageString);
                Log.e("attendanceJSON",searchFaceJSON.toString());
            } catch (JSONException e) {
                Log.e("JSON Exception", e.toString());
            }

            String searchFaceResponse = "";

            try {
                searchFaceResponse = post(searchFaceUrl, searchFaceJSON.toString());
                Log.i("searchFace response",searchFaceResponse);
            }
            catch (IOException e) {
                Log.i("heroku exception",e.toString());
            }
            return searchFaceResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Response from server: " + result);

            if (pd!=null) {
                pd.dismiss();
            }

            JSONObject jsonResultWithFaceIds = new JSONObject();

            try {
                jsonResultWithFaceIds = new JSONObject(result);
            }
            catch (org.json.JSONException je) {
                Log.e("JSON Exception faceids", je.toString());
            }

            try {
                JSONArray faceids = jsonResultWithFaceIds.getJSONArray("faces");
                Log.e("Extracted faces",faceids.toString());

                pd2 = new ProgressDialog(UploadActivity.this);
                pd2.setTitle("Sending Extracted Face IDs");
                pd2.setMessage(faceids.toString());
                pd2.setCancelable(false);
                pd2.setIndeterminate(true);
                pd2.show();
                //showAlert("Extracted face ids ",faceids.toString());
                new MarkAttendance(faceids).execute();
            }
            catch (org.json.JSONException je) {
                Log.e("JSON Exception faceids", je.toString());
            }

            // showing success response in an alert dialog
            //showAlert("Response from Server ",result);
            Log.e("Response from Server",result);
            super.onPostExecute(result);
        }

    }

     // Uploading the file to server
    private class MarkAttendance extends AsyncTask<Void, Integer, String> {

        JSONArray faceids = new JSONArray();

        protected MarkAttendance(JSONArray faceids) {
             this.faceids = faceids;
        }

        @Override
        protected void onPreExecute() {

//            pd = new ProgressDialog(UploadActivity.this);
//            pd.setTitle("Marking Attendance");
//            pd.setMessage("Please Wait...");
//            pd.setCancelable(false);
//            pd.setIndeterminate(true);
//            pd.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            if (isInternetAvailable())
                return uploadFile();
            else {
                toaster("Internet connection not available, your data has been saved for uploading later.");
                return "Couldn't establish a reliable server connection. Will try again later!";
            }
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {

            String date = new SimpleDateFormat("dd-MM-yy", Locale.getDefault()).format(new Date());
//            String dateFormatted = " \""+ date +"\" "  ;
//            Log.e("Formatted Date",dateFormatted);

            // For SearchFaces
            JSONObject attendanceJSON = new JSONObject();
            try {
                attendanceJSON = new JSONObject();
                attendanceJSON.put("sheet_name","rajendra@somaiya.edu");
                attendanceJSON.put("worksheet_name","ITC");
                attendanceJSON.put("face_ids",faceids);
                attendanceJSON.put("column_header",date);
                Log.e("attendanceJSON",attendanceJSON.toString());
            } catch (JSONException e) {
                Log.e("JSON Exception", e.toString());
            }

            String heroku_response = "";

            try {
                heroku_response = post(markAttendanceUrl, attendanceJSON.toString());
                Log.i("response heroku",heroku_response);
            }
            catch (IOException e) {
                Log.i("heroku exception",e.toString());
            }
            return heroku_response;
        }

        @Override
        protected void onPostExecute(String result) {

            Log.e(TAG, "Response from server: " + result);

            if (pd2!=null) {
                pd2.dismiss();
            }

            try {
                JSONObject resultObject = new JSONObject(result);
                String noStudents = resultObject.getString("total_students_marked");
                String responseString = noStudents + " students are present in your lecture today.";
                showAlert("Attendance Marked ",responseString);
            }
            catch (Throwable tx) {
                showAlert("Response from Server ",result);
            }
            // showing success response in an alert dialog
//            showAlert("Response from Server ",result);

            super.onPostExecute(result);
        }

    }

    /* Helper methods*/

    // Method to show alert dialog
    private void showAlert(String header, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setTitle(header)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // do nothing
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    // to check if internet access is avaialble
    public boolean isInternetAvailable() {
        try {
            final InetAddress address = InetAddress.getByName("www.google.com");
            return !address.equals("");
        } catch (UnknownHostException e) {
            Log.i("Internet status","Internet access not available");
        }
        return false;
    }

    // to check if all parameters have been selected before uploading data
    private boolean checkParams() {
        return (semesterSelector.getSelectedItem()!= null && subjectSelector.getSelectedItem()!=null && divSelector.getSelectedItem()!=null);
    }

    // to display toast on nonUI threads
    public void toaster(final String message) {
        UploadActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(UploadActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    // to check average brightness (light level) of image
    private int brightnesschecker(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] pic = new int[width*height];
        bitmap.getPixels(pic, 0, width, 0, 0, width, height);

        int R, G, B, Sum = 0;

        for (int y=0; y<height; y++) {
            for (int x=0; x<width; x++) {
                int index = y*width + x;
                R = (pic[index] >> 16) & 0xff;
                G = (pic[index] >> 8) & 0xff;
                B = (pic[index]) & 0xff;
                //Log.e("RGB values",Integer.toString(R)+" "+Integer.toString(G)+" "+Integer.toString(B));
                Sum = Sum + R + G + B;
            }
        }
        int level = Sum/(3*width*height);

        if (level <= 40) {
            brightnessLevel.setText("Too less light! Consider retaking photo! ("+ Integer.toString(level)+")");
            brightnessLevel.setTextColor(Color.rgb(226,90,23));
        }
        else if (level> 40 && level <=140){
            brightnessLevel.setText("Photo seems fine! ("+ Integer.toString(level)+")");
            brightnessLevel.setTextColor(Color.rgb(87,205,19));
        }
        else{
            brightnessLevel.setText("Photo too bright! Consider retaking photo! ("+ Integer.toString(level)+")");
            brightnessLevel.setTextColor(Color.rgb(226,90,23));
        }
        return level;
    }

    // OkHTTP post method
    private String post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    // to convert bitmap objects to base64 string
    private String base64Converter () {
        // Converting image to base64 encoded string
        Bitmap bm = BitmapFactory.decodeFile(filePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] img64 = baos.toByteArray();
        encodedImageString = Base64.encodeToString(img64, Base64.DEFAULT);
        Log.i("img64string",encodedImageString);

        return encodedImageString;
    }

    // to draw bounded boxes on identified faces
    private Bitmap drawRects (String filepath, BitmapFactory.Options options) {

        Bitmap facebm = BitmapFactory.decodeFile(filepath);

        Paint myRectPaint = new Paint();
        myRectPaint.setStrokeWidth(5);
        myRectPaint.setColor(Color.GREEN);
        myRectPaint.setStyle(Paint.Style.STROKE);

        Bitmap tempBitmap = Bitmap.createBitmap(facebm.getWidth(), facebm.getHeight(), Bitmap.Config.RGB_565);
        Canvas tempCanvas = new Canvas(tempBitmap);
        tempCanvas.drawBitmap(facebm, 0, 0, null);

        FaceDetector faceDetector = new FaceDetector.Builder(getApplicationContext()).setTrackingEnabled(false).build();
        if(!faceDetector.isOperational()){
            //showAlert("Sorry!","Could not set up the face detector!");
            return facebm;
        }

        Frame frame = new Frame.Builder().setBitmap(facebm).build();
        SparseArray<Face> faces = faceDetector.detect(frame);
        if (faces.size()>0) {
            faceCount.setText("Number of faces detected: "+faces.size());
        }
        else {
            faceCount.setText(" ");
        }
        Log.e("Number faces detected",""+faces.size());

        for(int i=0; i<faces.size(); i++) {
            Face thisFace = faces.valueAt(i);
            float x1 = thisFace.getPosition().x;
            float y1 = thisFace.getPosition().y;
            float x2 = x1 + thisFace.getWidth();
            float y2 = y1 + thisFace.getHeight();
            tempCanvas.drawRoundRect(new RectF(x1, y1, x2, y2), 2, 2, myRectPaint);
        }

        return tempBitmap;
    }

    private String reScaleAndEncode (Bitmap bm, int newWidth) {
        float aspectRatio = bm.getWidth() /
                (float) bm.getHeight();
        int newHeight = Math.round(newWidth / aspectRatio);

        Bitmap reScaledBitmap = Bitmap.createScaledBitmap(
                bm, newWidth, newHeight, false);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        reScaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] img64 = baos.toByteArray();
        encodedImageString = Base64.encodeToString(img64, Base64.DEFAULT);
        Log.i("rescaled img64string",encodedImageString);
        return encodedImageString;
    }



}