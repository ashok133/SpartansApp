package com.example.apk.spartans;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import static android.R.attr.data;
import static java.lang.Math.abs;

public class MainActivity extends Activity{

    // LogCat tag
    private static final String TAG = MainActivity.class.getSimpleName();

    // Camera activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    private static final int PICK_PHOTO_FOR_UPLOAD = 300;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    AlertDialog wipeAlert;
    CharSequence[] values = {" Older than a week "," Older than a month ", " All Images "};

    // server url for uploading
    public String fileUploadUrl;

    private static Uri fileUri; // file url to store image/video

    private ImageButton btnCapturePicture, btnPickImage, wipecache, openAttendance, btnRecordVideo, manualMarking;
    private EditText ipAddress;

    public int noDays=999;

    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        //ipAddress = findViewById(R.id.serverip);

        btnCapturePicture = findViewById(R.id.captureImageCircle);
        btnPickImage = findViewById(R.id.pickImageCircle);
        btnRecordVideo = findViewById(R.id.videoCaptureCircle);
        wipecache = findViewById(R.id.wipeCacheCircle);
        //changeIp = findViewById(R.id.changeIPCircle);
        openAttendance = findViewById(R.id.openAttendanceCircle);
        manualMarking = findViewById(R.id.manualCircle);

//        changeIp.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                //set new IP entered by user
//                //first check if IP is not null
//                Intent intent = new Intent(MainActivity.this, Config.class);
//                startActivity(intent);
//            }
//        });

        btnPickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickPhoto();
            }
        });

        manualMarking.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //set new IP entered by user
                //first check if IP is not null
                Intent intent = new Intent(MainActivity.this, ManualMarking.class);
                startActivity(intent);
            }
        });

        /**
         * Capture image button click event
         */
        btnCapturePicture.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // capture picture
                captureImage();
            }
        });

        /**
         * Record video button click event
         */
        btnRecordVideo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // record video
                recordVideo();
            }
        });

        // to clear media cache after one week
        wipecache.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                createDialogBoxWithOptions (values) ;
                //createDialogBoxWithOptions (values);
            }
        });

        openAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Open Sheet of that teacher
                openApp("com.google.android.apps.docs.editors.sheets");
            }
        } );

        // Checking camera availability
        if (!isDeviceSupportCamera()) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Your device doesn't support a camera",
                    Toast.LENGTH_LONG).show();
            // will close the app if the device doesn't have camera
            finish();
        }
    }

    /**
     * Checking if device has camera hardware
     * */
    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public void pickPhoto() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_UPLOAD);

    }

    /**
     * Launching camera app to capture image
     */
    public void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        Log.i("FileURL",fileUri.toString());

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Launching camera app to record video
     */
    private void recordVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);

        // set video quality
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        // set the image file name
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the video capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    /**
     * Here we store the file url as it will be null after returning from camera
     * app
     */

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    /* *
     * Receiving activity result method will be called after closing the camera
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        // if the result is capturing Image
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {

                // successfully captured the image
                // launching upload activity
                launchUploadActivity(true);


            } else if (resultCode == RESULT_CANCELED) {

                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();

            } else if (requestCode == PICK_PHOTO_FOR_UPLOAD) {
                if (data == null) {
                    //Display an error
                    return;
                }
                //fileUri = data.getData();
                String tempFileUri = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                fileUri = android.net.Uri.parse(tempFileUri);
                Log.e("Taken filepath",fileUri.toString());
                launchUploadActivity(true);
                //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
            }

            else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }

        } else if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {

                // video successfully recorded
                // launching upload activity
                launchUploadActivity(false);

            } else if (resultCode == RESULT_CANCELED) {

                // user cancelled recording
                Toast.makeText(getApplicationContext(),
                        "User cancelled video recording", Toast.LENGTH_SHORT)
                        .show();

            } else {
                // failed to record video
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to record video", Toast.LENGTH_SHORT)
                        .show();
            }
        }

    }

    private void launchUploadActivity(boolean isImage){
        Intent i = new Intent(MainActivity.this, UploadActivity.class);
        i.putExtra("filePath", fileUri.getPath());
        i.putExtra("isImage", isImage);
        startActivity(i);
    }

    /* Helper Methods */

    // for opening attendance sheet
    private void openApp(String packageName) {
        if (isAppInstalled(this, packageName)) {
            startActivity(getPackageManager().getLaunchIntentForPackage(packageName));
        }
        else {
            // open browser with sheets link if app not installed
            String sheetsurl = "https://docs.google.com/spreadsheets/d/1CIxnDItXLp1BhNHP979qBR-2fO4pLHc9LCEkCppJnDg/edit?usp=sharing";
            Intent browserIntent = new Intent(Intent.ACTION_VIEW);
            browserIntent.setData(Uri.parse(sheetsurl));
            startActivity(browserIntent);
        }
    }

    // to check if app is installed or not
    public static boolean isAppInstalled(Activity activity, String packageName) {
        PackageManager packageManager = activity.getPackageManager();
        try {
            packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true ;
        }
        catch (PackageManager.NameNotFoundException e) {
            Log.e("App Status","App not found");
        }
        return false;
    }

    // to create file uri to store captured image/video
    public Uri getOutputMediaFileUri(int type) {
        requestRuntimePermission();
        return Uri.fromFile(getOutputMediaFile(type));
    }

    // requesting runtime permission for writing to storage
    public void requestRuntimePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
    }

    // to return image/video from uri
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                Config.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed to create "
                        + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private void createDialogBoxWithOptions(CharSequence[] values){

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setTitle("Wipe Images ");

        builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
//                int noDays = 0;
                switch(item)
                {
                    case 0:
                        //noDays = 0;
                        cleanCache(7);
                        break;
                    case 1:
                        //noDays = 7;
                        cleanCache(30);
                        break;
                    case 2:
                        //noDays = 30;
                        cleanCache(0);
                        break;
                    default:
                        noDays = 0;
                }
                wipeAlert.dismiss();
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            public void onClick (DialogInterface dialog, int item) {
                wipeAlert.dismiss();
            }
        });


        wipeAlert = builder.create();
        wipeAlert.show();

        //while(wipeAlert!=null)
        //return noDays;
    }

    public void cleanCache(int noDays) {
        File mediaStorageDir = new File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Config.IMAGE_DIRECTORY_NAME);
        File[] files = mediaStorageDir.listFiles();
        Log.i("Files","Size: "+files.length);

        int delFilesCount = 0 ;

        for (int i=0; i<files.length; i++)
        {
            Log.i("Files","FileName: "+files[i].getName());
            String currTimeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            Log.i("Current TS",""+currTimeStamp);
            int currDay = Integer.parseInt(currTimeStamp.substring(6,8));
            int fileDay = Integer.parseInt(files[i].getName().substring(10,12));
            Log.i("Current Day",""+currDay);
            Log.i("File Day",""+fileDay);
            int age = (currDay - fileDay);
            Log.i("Age of file","Age:"+age);
            if (abs(age)>=noDays)
            {
                Log.i("Age of deleted file","Age:"+age);
                files[i].delete();
                delFilesCount++;
            }
        }

        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, delFilesCount+" Files Deleted Successfully", Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}