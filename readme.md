<h1>Client side native Android app for attendance monitoring</h1>

v0.1:
- Upload Image base64 encoding
- Upload Video
- Wipe Cache

v0.2:
- Check for Internet Connection and defer upload if not connected
- Removed Video Upload 
- Added more POST parameters for server side calls
- Manually set Server IP

v0.3:
- Improved UX - A fresh coat of paint and better overall experience 
- Added ‘Open Attendance’ functionality - Teacher can open her own sheet any time she wants to view, and modify attendance manually with right privileges. Sheets/Drive app is opened if installed else opened in default browser.
- Added 'Manual Attendance' functionality - Teacher can enter absentee roll numbers in case none of the primary modes of sourcing image work.
- Added another threshold for lighting check - 3 results possible now: Too less light, Okay, Too much light
- Set up sequential selection of semester, division and subject - Upload button stays disabled until all parameters are selected
- MarkAttendance API integration successful
- Deleted previous file if retaking a picture

v0.4:
- SearchFace API integration successful
- Chained SearchFace and MarkAttendance APIs for a complete flow of attendance marking
- Added in-app face detection using Mobile Vision API. The teacher gets to know the number of faces before uploading
- Refactored some code 

v0.5:
- Added Submit button for Manual Marking submission
- Fixed layouts of Upload Activity
- Updated Change IP activity for new API URLs
- Updated Manual Marking thumbnail in Main Activity
- Cleaned and refactored more code 

v0.6: 
- Added 'Pick Image' functionality - To pick images from other sources
- Fixed image scaling issue that unnecesarily downscaled bitmaps
- Removed annoying toast messages

v0.7:
- Added resolution check before trying to generate base64. High res images can now be handled by the app.

v1.0:
- Revised API URLs in app (backend now deployed on an EC2 instance)
- Removed 'Change IP' - Not needed anymore
- Added option for user to select number of days for file age before wiping cache
- Cleaned server response alert dialogs with more human understandable messages